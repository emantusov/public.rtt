//import axiosInstance from "@/axios";
import * as mock from '@/mock/mock'

export default {
    // eslint-disable-next-line no-unused-vars
    DELETE_RTT: async (context, payload) => {
        /*axiosInstance.post('/index.php', {
            action: 'rtt:delete',
            params: {rttId: payload.rttId, userId: payload.userId}
        }).then(
            (response) => {
                context.commit('SET_RTT_LIST', response.data.rttList)
            }
        ).catch(() => {
            context.commit('SET_RTT_LIST', [])
        })*/
    },
    // eslint-disable-next-line no-unused-vars
    LOAD_RTT_ITEM: async (context, payload) => {
       /* axiosInstance.post('/index.php', {
            action: 'rtt:getById',
            params: {rttId: payload.rttId}
        }).then(
            (response) => {
                context.commit('SET_RTT_ITEM', response.data.groups)
            }
        ).catch(() => {
            context.commit('SET_RTT_ITEM', [])
        })*/
        context.commit('SET_RTT_ITEM', mock.rttItem)
    },
    LOAD_RTT_TEMPLATE: async (context) => {
       /* axiosInstance.post('/index.php', {
            action: 'rtt:getTemplate',params: {}
        }).then(
            (response) => {
                console.log(response.data.template)
                context.commit('SET_RTT_ITEM', response.data.groups)
            }
        ).catch(() => {
            context.commit('SET_RTT_ITEM', [])
        })*/
        context.commit('SET_RTT_ITEM', mock.newTemplate)
    },
    // eslint-disable-next-line no-unused-vars
    LOAD_RTT_LIST: async (context, payload) => {
       /* axiosInstance.post('/index.php', {
            action: 'rtt:getList',params: {userId: payload.userId}
        }).then(
            (response) => {
                context.commit('SET_RTT_LIST', response.data.rttList)
            }
        ).catch(() => {
            context.commit('SET_RTT_LIST', [])
        })*/
        context.commit('SET_RTT_LIST', mock.rttList)

    },
    // eslint-disable-next-line no-unused-vars
    SAVE_RTT: async (context, payload) => {
        /*let formData = new FormData();
        payload.fields.forEach((item) => {
            if (!item.new) {
                return
            }
            if (item.type === 'file') {
                item.value.forEach((file) => {
                    formData.append(`${item.fieldId}[]`, file)
                })
            }else if(item.type === 'checkbox') {
                item.value.forEach((checkbox) => {
                    formData.append(`${item.fieldId}[]`, checkbox)
                })
            } else {
                formData.append(item.fieldId, item.value)
            }
        })
        formData.append("action", "rtt:save")
        axiosInstance.post('/index.php', formData,{headers: {
                'Content-Type': 'multipart/form-data'
            }}).then(
            (response) => {
                context.commit('SET_RTT_LIST', response.data.rttList)
            }
        ).catch(() => {
            context.commit('SET_RTT_LIST', [])
        })*/
        context.commit('SET_RTT_LIST', mock.rttList)
    },
    GET_USER_ID: async (context) => {
        /*axiosInstance.post('/index.php', {
            action: 'user:getId',
            params: {}
        }).then(
            (response) => {
                context.commit('SET_USER_ID', response.data.userId)
            }
        ).catch(() => {
        })*/
        context.commit('SET_USER_ID', mock.userId)
    },
}