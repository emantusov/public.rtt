import {createStore} from 'vuex'
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import state from "@/store/state";


const store = createStore({
    state,
    mutations,
    actions
})

export default store