export default {
    SET_RTT_LIST: (state, rttList) => {
        state.rttList = rttList
    },
    SET_RTT_ITEM: (state, groups) => {
        state.rttItem.groups = groups
    },
    CLEAR_RTT_ITEM: (state) => {
        state.rttItem.groups = []
    },
    SET_USER_ID: (state, userId) => {
        state.userId = userId
    },
}