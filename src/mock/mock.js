const rttList = [
    {
        "id": "3159",
        "name": "name",
        "address": "erer",
        "phone": "+7 (232) 32-32-323",
        "image": "https://metalpart.ru/upload/iblock/295/img2369.jpg"
    },
    {
        "id": "3160",
        "name": "name 2",
        "address": "dsdsd",
        "phone": "+7 (232) 32-32-323",
        "image": "https://metalpart.ru/upload/iblock/c86/img2369.jpg"
    },
    {
        "id": "3168",
        "name": "name 3",
        "address": "3",
        "phone": "+7 (333) 33-33-333",
        "image": "https://metalpart.ru/upload/iblock/44f/img2369.jpg"
    },
    {
        "id": "3214",
        "name": "name 123",
        "address": "dsdsd",
        "phone": "12323",
        "image": "https://metalpart.ru/upload/iblock/44f/img2369.jpg"
    }];
const userId = 1024;

const rttItem = [
        {
            "id": 1,
            "name": "Информация о магазине для вывода в раздел \"Где Купить\"",
            "fields": [
                {
                    "id": "355",
                    "name": "Название (Вывеска)",
                    "sort": 12,
                    "type": "text",
                    "multiple": "N",
                    "value": "Название (Вывеска) 123"
                },
                {
                    "id": "419",
                    "name": "Фото улицы, где расположен магазин",
                    "sort": 15,
                    "type": "file",
                    "multiple": "Y",
                    "value": ["https://metalpart.ru/upload/iblock/7b1/Kids_G.jpg"]
                },
                {
                    "id": "420",
                    "name": "Фото входной группы",
                    "sort": 17,
                    "type": "file",
                    "multiple": "Y",
                    "value": ["https://metalpart.ru/upload/iblock/91f/1200x540px_Brands_M_2.jpg", "https://metalpart.ru//upload/iblock/5d0/1200x540px_Brands_M_1.jpg"]
                },
                {
                    "id": "421",
                    "name": " Фото торгового зала (1 фото общее + 2 фото с разных ракурсов)",
                    "sort": 18,
                    "type": "file",
                    "multiple": "Y",
                    "value": ["https://metalpart.ru/upload/iblock/b25/Kids_G.jpg", "https://metalpart.ru/upload/iblock/b2e/Kids_Baby.jpg"]
                },
                {
                    "id": "422",
                    "name": "Фото витрины с продукцией Металпарт",
                    "sort": 19,
                    "type": "file",
                    "multiple": "Y",
                    "value": ["https://metalpart.ru/upload/iblock/7b6/1200x540px_Brands_K_3.jpg"]
                },
                {
                    "id": "423",
                    "name": "Кассовая зона",
                    "sort": 20,
                    "type": "file",
                    "multiple": "Y",
                    "value": ["https://metalpart.ru/upload/iblock/a67/Sport_M.jpg", "https://metalpart.ru/upload/iblock/d46/Sport_W.jpg"]
                },
                {
                    "id": "356",
                    "name": "Юридическое лицо",
                    "sort": 30,
                    "type": "text",
                    "multiple": "N",
                    "value": "Юридическое лицо"
                },
                {"id": "357", "name": "Адрес", "sort": 40, "type": "textarea", "multiple": "N", "value": "Адрес"},
                {"id": "358", "name": "E-mail", "sort": 50, "type": "text", "multiple": "N", "value": "w@ua.ru"},
                {"id": "359", "name": "Телефон", "sort": 60, "type": "text", "multiple": "N", "value": "12323"},
                {
                    "id": "360",
                    "name": "Сайт (если есть)",
                    "sort": 70,
                    "type": "text",
                    "multiple": "N",
                    "value": "Сайт (если есть)"
                },
                {
                    "id": "362",
                    "name": "Время работы",
                    "sort": 90,
                    "type": "text",
                    "multiple": "N",
                    "value": "Время работы"
                },
                {
                    "id": "384",
                    "name": "ИНН",
                    "sort": 150,
                    "type": "text",
                    "multiple": "N",
                    "value": "22222222222222222"
                }], "from": 1, "to": 999
        },
        {
            "id": 2,
            "name": "Экономические вопросы:",
            "fields": [
                {
                    "id": "363",
                    "name": "Площадь магазина, кв. м.",
                    "sort": 1100,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "1675",
                    "values": [{"id": "1674", "text": "до 50 м"}, {"id": "1675", "text": "от 50 до 100"}, {
                        "id": "1676",
                        "text": "более ста кв"
                    }]
                },
                {
                    "id": "364",
                    "name": "Направления деятельности",
                    "sort": 1110,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [1668, 1670, 1672],
                    "values": [{"id": "1670", "text": "Автохимия, автомасла"}, {
                        "id": "1671",
                        "text": "Ремонт и техническое обслуживание"
                    }, {"id": "1673", "text": "Автоэлектроника и осветительное оборудование"}, {
                        "id": "1668",
                        "text": "Запасные части"
                    }, {"id": "1669", "text": "Внедорожное оборудование и аксессуары"}, {
                        "id": "1672",
                        "text": "Диски и шины"
                    }]
                }, {
                    "id": "365",
                    "name": "Комплектующими для каких автомобилей Вы занимаетесь?",
                    "sort": 1120,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [1680, 1679],
                    "values": [{"id": "1679", "text": "ГАЗ"}, {"id": "1680", "text": "КАМАЗ"}, {
                        "id": "1677",
                        "text": "УАЗ"
                    }, {"id": "1678", "text": "ВАЗ"}]
                }, {
                    "id": "366",
                    "name": "У какого (каких) дистрибьютора(ов) закупаете запчасти MetalPart?",
                    "sort": 1130,
                    "type": "text",
                    "multiple": "N",
                    "value": "У какого (каких) дистриб"
                }, {
                    "id": "385",
                    "name": "Обращаются ли к вам по гарантийным случаям с продукцией МП?",
                    "sort": 1160,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "1681",
                    "values": [{"id": "1682", "text": "Нет"}, {"id": "1681", "text": "Да"}]
                }, {
                    "id": "386",
                    "name": "Периодичность поставок продукции МеталПарт",
                    "sort": 1170,
                    "type": "text",
                    "multiple": "N",
                    "value": "Нет Периодичность поставок продук"
                }, {
                    "id": "387",
                    "name": "Какой объём продаж в месяц  Металпарт?",
                    "sort": 1180,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "1685",
                    "values": [{"id": "1685", "text": "от 30 до 50тыс"}, {
                        "id": "1686",
                        "text": "Более 50тыс"
                    }, {"id": "1683", "text": "До 10тыс"}, {"id": "1684", "text": "от 10 до 30тыс"}]
                }, {
                    "id": "388",
                    "name": "Сколько наименований запчастей Металпарт у вас в магазине?",
                    "sort": 1190,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "1687",
                    "values": [{"id": "1690", "text": "Более 50тыс"}, {"id": "1687", "text": "До 10тыс"}, {
                        "id": "1689",
                        "text": "до 50тыс"
                    }, {"id": "1688", "text": "до 30тыс"}]
                }, {
                    "id": "389",
                    "name": "Относится ли Ваш магазин к сети магазинов?(Если да,то сколько магазинов  всети)",
                    "sort": 1200,
                    "type": "text",
                    "multiple": "N",
                    "value": "тносится ли Ваш магазин к сети магазинов?(Ес"
                }, {
                    "id": "390",
                    "name": "Площадь магазина",
                    "sort": 1210,
                    "type": "text",
                    "multiple": "N",
                    "value": "ощадь магазина"
                }, {
                    "id": "394",
                    "name": "Количество продавцов",
                    "sort": 1250,
                    "type": "text",
                    "multiple": "N",
                    "value": "Количество продавцов"
                }, {
                    "id": "395",
                    "name": "Сколько примерно наименований всего товаров у ВАС представлены в магазине?",
                    "sort": 1260,
                    "type": "text",
                    "multiple": "N",
                    "value": "Сколько примерно наимено"
                }, {
                    "id": "396",
                    "name": "Сколько примерно позиций для УАЗА представлены у Вас в магазине?",
                    "sort": 1270,
                    "type": "text",
                    "multiple": "N",
                    "value": "Сколько примерно позиций"
                }, {
                    "id": "397",
                    "name": "Стоит ли у Вас фирменное торговое оборудование каких-либо брендов? Если да ,то какое?",
                    "sort": 1280,
                    "type": "text",
                    "multiple": "N",
                    "value": "Стоит ли у Вас фирменно"
                }, {
                    "id": "399",
                    "name": "Какие есть варианты оплаты у вас в магазине?",
                    "sort": 1300,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [1702, 1703],
                    "values": [{"id": "1703", "text": "Карта"}, {"id": "1702", "text": "Наличные"}]
                }, {
                    "id": "400",
                    "name": "Есть ли доставка?",
                    "sort": 1310,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "1705",
                    "values": [{"id": "1705", "text": "Да"}, {"id": "1704", "text": "Нет"}]
                }], "from": 1000, "to": 1999
        }, {
            "id": 3,
            "name": "Вопросы про МеталПарт",
            "fields": [{
                "id": "398",
                "name": "Хотите ли получить фирм торговое оборудование Металпарт",
                "sort": 2290,
                "type": "radiobutton",
                "multiple": "N",
                "value": "1700",
                "values": [{"id": "1700", "text": "Да"}, {"id": "1701", "text": "Нет"}]
            }],
            "from": 2000,
            "to": 2999
        }, {
            "id": 4,
            "name": "Соц.сети, сто, парковка трассы и остальные",
            "fields": [{
                "id": "361",
                "name": "Соцсети (если есть)",
                "sort": 3080,
                "type": "text",
                "multiple": "N",
                "value": "Соцсети (если есть)"
            }, {
                "id": "391",
                "name": "Есть ли парковка возле магазина?",
                "sort": 3220,
                "type": "radiobutton",
                "multiple": "N",
                "value": "1692",
                "values": [{"id": "1691", "text": "Да собственная"}, {
                    "id": "1692",
                    "text": "Да,общественная"
                }, {"id": "1693", "text": "Нет"}]
            }, {
                "id": "392",
                "name": "Как близко Ваш магазин к федеральным трассам?",
                "sort": 3230,
                "type": "radiobutton",
                "multiple": "N",
                "value": "1696",
                "values": [{"id": "1695", "text": "Находится в центре города"}, {
                    "id": "1694",
                    "text": "Стоит возле трассы"
                }, {"id": "1696", "text": "Находится на окраине города"}]
            }, {
                "id": "393",
                "name": "Есть ли рядом СТО?",
                "sort": 3240,
                "type": "radiobutton",
                "multiple": "N",
                "value": "1698",
                "values": [{"id": "1699", "text": "Нет"}, {
                    "id": "1698",
                    "text": "Есть партнёрские отношения"
                }, {"id": "1697", "text": "Есть, относится к магазину"}]
            }, {
                "id": "401",
                "name": "Если есть доставка, какие варианты вы используете?",
                "sort": 3320,
                "type": "checkbox",
                "multiple": "Y",
                "value": [1707, 1708],
                "values": [{"id": "1706", "text": "Самовывоз"}, {"id": "1708", "text": "Сдэк"}, {
                    "id": "1707",
                    "text": "Курьер"
                }]
            }],
            "from": 3000,
            "to": 3999
        }];
const newTemplate = [
        {
            "id": 1,
            "name": "Информация о магазине для вывода в раздел \"Где Купить\"",
            "fields": [
                {
                    "id": "355",
                    "name": "Название (Вывеска)",
                    "sort": 12,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                },
                {
                    "id": "419",
                    "name": "Фото улицы, где расположен магазин",
                    "sort": 15,
                    "type": "file",
                    "multiple": "Y",
                    "value": []
                },
                {
                    "id": "420",
                    "name": "Фото входной группы",
                    "sort": 17,
                    "type": "file",
                    "multiple": "Y",
                    "value": []
                },
                {
                    "id": "421",
                    "name": " Фото торгового зала (1 фото общее + 2 фото с разных ракурсов)",
                    "sort": 18,
                    "type": "file",
                    "multiple": "Y",
                    "value": []
                },
                {
                    "id": "422",
                    "name": "Фото витрины с продукцией Металпарт",
                    "sort": 19,
                    "type": "file",
                    "multiple": "Y",
                    "value": []
                },
                {
                    "id": "423",
                    "name": "Кассовая зона",
                    "sort": 20,
                    "type": "file",
                    "multiple": "Y",
                    "value": []
                },
                {
                    "id": "356",
                    "name": "Юридическое лицо",
                    "sort": 30,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                },
                {"id": "357", "name": "Адрес", "sort": 40, "type": "textarea", "multiple": "N", "value": ""},
                {"id": "358", "name": "E-mail", "sort": 50, "type": "text", "multiple": "N", "value": ""},
                {"id": "359", "name": "Телефон", "sort": 60, "type": "text", "multiple": "N", "value": ""},
                {
                    "id": "360",
                    "name": "Сайт (если есть)",
                    "sort": 70,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                },
                {
                    "id": "362",
                    "name": "Время работы",
                    "sort": 90,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                },
                {
                    "id": "384",
                    "name": "ИНН",
                    "sort": 150,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }], "from": 1, "to": 999
        },
        {
            "id": 2,
            "name": "Экономические вопросы:",
            "fields": [
                {
                    "id": "363",
                    "name": "Площадь магазина, кв. м.",
                    "sort": 1100,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "",
                    "values": [{"id": "1674", "text": "до 50 м"}, {"id": "1675", "text": "от 50 до 100"}, {
                        "id": "1676",
                        "text": "более ста кв"
                    }]
                },
                {
                    "id": "364",
                    "name": "Направления деятельности",
                    "sort": 1110,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [],
                    "values": [{"id": "1670", "text": "Автохимия, автомасла"}, {
                        "id": "1671",
                        "text": "Ремонт и техническое обслуживание"
                    }, {"id": "1673", "text": "Автоэлектроника и осветительное оборудование"}, {
                        "id": "1668",
                        "text": "Запасные части"
                    }, {"id": "1669", "text": "Внедорожное оборудование и аксессуары"}, {
                        "id": "1672",
                        "text": "Диски и шины"
                    }]
                }, {
                    "id": "365",
                    "name": "Комплектующими для каких автомобилей Вы занимаетесь?",
                    "sort": 1120,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [],
                    "values": [{"id": "1679", "text": "ГАЗ"}, {"id": "1680", "text": "КАМАЗ"}, {
                        "id": "1677",
                        "text": "УАЗ"
                    }, {"id": "1678", "text": "ВАЗ"}]
                }, {
                    "id": "366",
                    "name": "У какого (каких) дистрибьютора(ов) закупаете запчасти MetalPart?",
                    "sort": 1130,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "385",
                    "name": "Обращаются ли к вам по гарантийным случаям с продукцией МП?",
                    "sort": 1160,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "",
                    "values": [{"id": "1682", "text": "Нет"}, {"id": "1681", "text": "Да"}]
                }, {
                    "id": "386",
                    "name": "Периодичность поставок продукции МеталПарт",
                    "sort": 1170,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "387",
                    "name": "Какой объём продаж в месяц  Металпарт?",
                    "sort": 1180,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "",
                    "values": [{"id": "1685", "text": "от 30 до 50тыс"}, {
                        "id": "1686",
                        "text": "Более 50тыс"
                    }, {"id": "1683", "text": "До 10тыс"}, {"id": "1684", "text": "от 10 до 30тыс"}]
                }, {
                    "id": "388",
                    "name": "Сколько наименований запчастей Металпарт у вас в магазине?",
                    "sort": 1190,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "",
                    "values": [{"id": "1690", "text": "Более 50тыс"}, {"id": "1687", "text": "До 10тыс"}, {
                        "id": "1689",
                        "text": "до 50тыс"
                    }, {"id": "1688", "text": "до 30тыс"}]
                }, {
                    "id": "389",
                    "name": "Относится ли Ваш магазин к сети магазинов?(Если да,то сколько магазинов  всети)",
                    "sort": 1200,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "390",
                    "name": "Площадь магазина",
                    "sort": 1210,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "394",
                    "name": "Количество продавцов",
                    "sort": 1250,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "395",
                    "name": "Сколько примерно наименований всего товаров у ВАС представлены в магазине?",
                    "sort": 1260,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "396",
                    "name": "Сколько примерно позиций для УАЗА представлены у Вас в магазине?",
                    "sort": 1270,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "397",
                    "name": "Стоит ли у Вас фирменное торговое оборудование каких-либо брендов? Если да ,то какое?",
                    "sort": 1280,
                    "type": "text",
                    "multiple": "N",
                    "value": ""
                }, {
                    "id": "399",
                    "name": "Какие есть варианты оплаты у вас в магазине?",
                    "sort": 1300,
                    "type": "checkbox",
                    "multiple": "Y",
                    "value": [],
                    "values": [{"id": "1703", "text": "Карта"}, {"id": "1702", "text": "Наличные"}]
                }, {
                    "id": "400",
                    "name": "Есть ли доставка?",
                    "sort": 1310,
                    "type": "radiobutton",
                    "multiple": "N",
                    "value": "",
                    "values": [{"id": "1705", "text": "Да"}, {"id": "1704", "text": "Нет"}]
                }], "from": 1000, "to": 1999
        }, {
            "id": 3,
            "name": "Вопросы про МеталПарт",
            "fields": [{
                "id": "398",
                "name": "Хотите ли получить фирм торговое оборудование Металпарт",
                "sort": 2290,
                "type": "radiobutton",
                "multiple": "N",
                "value": "",
                "values": [{"id": "1700", "text": "Да"}, {"id": "1701", "text": "Нет"}]
            }],
            "from": 2000,
            "to": 2999
        }, {
            "id": 4,
            "name": "Соц.сети, сто, парковка трассы и остальные",
            "fields": [{
                "id": "361",
                "name": "Соцсети (если есть)",
                "sort": 3080,
                "type": "text",
                "multiple": "N",
                "value": ""
            }, {
                "id": "391",
                "name": "Есть ли парковка возле магазина?",
                "sort": 3220,
                "type": "radiobutton",
                "multiple": "N",
                "value": "",
                "values": [{"id": "1691", "text": "Да собственная"}, {
                    "id": "1692",
                    "text": "Да,общественная"
                }, {"id": "1693", "text": "Нет"}]
            }, {
                "id": "392",
                "name": "Как близко Ваш магазин к федеральным трассам?",
                "sort": 3230,
                "type": "radiobutton",
                "multiple": "N",
                "value": "",
                "values": [{"id": "1695", "text": "Находится в центре города"}, {
                    "id": "1694",
                    "text": "Стоит возле трассы"
                }, {"id": "1696", "text": "Находится на окраине города"}]
            }, {
                "id": "393",
                "name": "Есть ли рядом СТО?",
                "sort": 3240,
                "type": "radiobutton",
                "multiple": "N",
                "value": "",
                "values": [{"id": "1699", "text": "Нет"}, {
                    "id": "1698",
                    "text": "Есть партнёрские отношения"
                }, {"id": "1697", "text": "Есть, относится к магазину"}]
            }, {
                "id": "401",
                "name": "Если есть доставка, какие варианты вы используете?",
                "sort": 3320,
                "type": "checkbox",
                "multiple": "Y",
                "value": [],
                "values": [{"id": "1706", "text": "Самовывоз"}, {"id": "1708", "text": "Сдэк"}, {
                    "id": "1707",
                    "text": "Курьер"
                }]
            }],
            "from": 3000,
            "to": 3999
        }];

export {userId, rttList, rttItem, newTemplate}