import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: 'https://metalpart.ru/rest/'
});

export default axiosInstance