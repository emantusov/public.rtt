import { createRouter, createWebHistory } from 'vue-router'
import RttList from '../views/RttList'
import RttDetail from '../views/RttDetail'

const routes = [
  {
    path: '/',
    name: 'RttList',
    component: RttList
  },
  {
    path: '/rtt/edit/:id',
    name: 'RttDetail',
    component: RttDetail
  },
  {
    path: '/rtt/add',
    name: 'RttAdd',
    component: RttDetail
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
